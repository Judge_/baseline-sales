import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { SmoothingModule } from './smoothing/smoothing.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    SmoothingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaselineSmoothingComponent } from './components/baseline-smoothing/baseline-smoothing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [BaselineSmoothingComponent],
  imports: [
    CommonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgChartsModule,
    MatCardModule,
  ],
  exports: [BaselineSmoothingComponent],
})
export class SmoothingModule {}

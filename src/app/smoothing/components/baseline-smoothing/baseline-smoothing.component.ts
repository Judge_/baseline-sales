import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ChartData } from 'chart.js';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { Subject, takeUntil } from 'rxjs';
import { ParsedSalesInput, SalesInput } from 'src/app/shared/models';
import { ExportCsvService } from 'src/app/shared/services/export-csv.service';
import { AlgorithmType } from '../../models';
import { SmoothingFunctionsService } from '../../services/smoothing-functions.service';

@Component({
  selector: 'app-baseline-smoothing',
  templateUrl: './baseline-smoothing.component.html',
  styleUrls: ['./baseline-smoothing.component.scss'],
})
export class BaselineSmoothingComponent implements OnInit, OnDestroy {
  public readonly AlgorithmType = AlgorithmType;
  public get algorithms() {
    return this.smoothingFunctionsService.getSmoothingAlgorithms();
  }

  public form = new FormGroup({
    csvInput: new FormControl<string | null>(null, Validators.required),
    algorithm: new FormControl<AlgorithmType>(
      AlgorithmType.MovingAverage,
      Validators.required
    ),
    periods: new FormControl<number>(3, [
      Validators.required,
      Validators.min(0),
      Validators.max(10),
    ]),
    smoothingFactor: new FormControl<number>({ value: 0.4, disabled: true }, [
      Validators.min(0),
      Validators.max(1),
    ]),
  });

  public chartData?: ChartData;

  private destroyed = new Subject();
  private parsedSales: ParsedSalesInput[] = [];
  private baselineValues: number[] = [];

  constructor(
    private readonly ngxCsvParser: NgxCsvParser,
    private readonly smoothingFunctionsService: SmoothingFunctionsService,
    private readonly exportCsvService: ExportCsvService
  ) {}

  public ngOnInit(): void {
    // Enable/Disable the smoothing factor field depending on algorithm
    this.form.controls.algorithm.valueChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe((algorithmType) => {
        if (
          this.algorithms.find(
            (algorithm) =>
              algorithm.algorithmType === algorithmType &&
              algorithm.usesSmoothingFactor
          )
        ) {
          this.form.controls.smoothingFactor.enable();
        } else {
          this.form.controls.smoothingFactor.disable();
        }
      });
  }

  fileChangeListener(event: Event): void {
    const input = event.target as HTMLInputElement;
    const files = input.files;

    if (!files?.length) {
      return;
    }

    this.ngxCsvParser
      .parse(files[0], {
        header: true,
        delimiter: ',',
        encoding: 'utf8',
      })
      .subscribe({
        next: (result: SalesInput[] | NgxCSVParserError) => {
          if (result instanceof NgxCSVParserError) {
            console.log('Error', result);
            return;
          }

          console.log('Result', result);
          this.parsedSales = result.map((value) => {
            return { Month: value.Month, Sales: parseFloat(value.Sales) };
          });
        },
        error: (error: NgxCSVParserError) => {
          console.log('Error', error);
        },
      });
  }

  public onSubmit(): void {
    if (this.form.invalid) {
      console.log('Invalid form: ', this.form);
      return;
    }

    if (this.parsedSales.length === 0) {
      console.log('No Records');
      return;
    }

    if (!(this.form.value.algorithm && this.form.value.periods)) {
      console.log('Empty Field');
      return;
    }

    // Trust the form validation
    this.baselineValues = this.smoothingFunctionsService.processAndSmoothSales(
      this.parsedSales,
      this.form.value.algorithm,
      {
        periods: this.form.value.periods,
        smoothingFactor: this.form.value.smoothingFactor ?? undefined,
      }
    );

    this.generateChart(this.parsedSales, this.baselineValues);
  }

  public exportCSV(): void {
    this.exportCsvService.exportBaselineData(
      this.parsedSales,
      this.baselineValues,
      this.form.value.csvInput ?? null
    );
  }

  public ngOnDestroy(): void {
    this.destroyed.next(null);
    this.destroyed.complete();
  }

  private generateChart(
    salesValues: ParsedSalesInput[],
    baselineValues: number[]
  ): void {
    this.chartData = {
      labels: salesValues.map((value) => value.Month),
      datasets: [
        {
          label: 'Sales',
          data: salesValues.map((value) => value.Sales),
          type: 'bar',
          backgroundColor: '#00a4d3',
          borderColor: '#00a4d3',
          order: 1,
        },
        {
          label: 'Baseline',
          data: baselineValues,
          type: 'line',
          backgroundColor: '#f18908',
          borderColor: '#f18908',
          order: 0,
        },
      ],
    };
  }
}

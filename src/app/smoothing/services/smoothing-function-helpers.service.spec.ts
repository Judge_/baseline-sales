import { TestBed } from '@angular/core/testing';

import { SmoothingFunctionHelpersService } from './smoothing-function-helpers.service';

describe('SmoothingFunctionHelpersService', () => {
  let service: SmoothingFunctionHelpersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmoothingFunctionHelpersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAverage should work for simple values', () => {
    const testInput: number[] = [50, 60, 40];

    expect(service.getAverage(testInput)).toEqual(50);
  });
});

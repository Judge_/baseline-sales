import { TestBed } from '@angular/core/testing';
import { SmoothingOptions } from '../models';

import { SingleExponentialService } from './single-exponential.service';

describe('SingleExponentialService', () => {
  let service: SingleExponentialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SingleExponentialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('apply should return correct Example 1 values', () => {
    const testInput: number[] = [50, 60, 40, 30, 90, -5, 40, 50, 30, 40];
    const testOptions: SmoothingOptions = { periods: 3, smoothingFactor: 0.4 };

    expect(service.apply(testInput, testOptions)).toEqual([
      50, 54, 48.4, 41.04, 60.623999999999995, 34.374399999999994, 36.62464,
      41.974784, 37.184870399999994, 38.31092224,
    ]);
  });

  it('apply should return correct Example 2 values', () => {
    const testInput: number[] = [15, 20, 16, 90, -90, -5, 17, 19, 16, 18];
    const testOptions: SmoothingOptions = { periods: 4, smoothingFactor: 0.5 };

    expect(service.apply(testInput, testOptions)).toEqual([
      35.25, 27.625, 21.8125, 55.90625, 0, 0, 8.5, 13.75, 14.875, 16.4375,
    ]);
  });
});

import { TestBed } from '@angular/core/testing';
import { SmoothingOptions } from '../models';

import { MovingAverageService } from './moving-average.service';

describe('MovingAverageService', () => {
  let service: MovingAverageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MovingAverageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('apply should return correct sample values', () => {
    const testInput: number[] = [50, 60, 40, 30, 90, -5, 40, 50, 30, 40];
    const testOptions: SmoothingOptions = { periods: 3 };

    expect(service.apply(testInput, testOptions)).toEqual([
      50, 55, 50, 43.333333333333336, 53.333333333333336, 38.333333333333336,
      41.666666666666664, 28.333333333333332, 40, 40,
    ]);
  });
});

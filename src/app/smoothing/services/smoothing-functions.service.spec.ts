import { TestBed } from '@angular/core/testing';

import { SmoothingFunctionsService } from './smoothing-functions.service';

describe('SmoothingFunctionsService', () => {
  let service: SmoothingFunctionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmoothingFunctionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

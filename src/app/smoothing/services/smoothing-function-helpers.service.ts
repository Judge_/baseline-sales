import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SmoothingFunctionHelpersService {
  public getAverage(numbersToAverage: number[]): number {
    return (
      numbersToAverage.reduce((acc, curr) => acc + curr) /
      numbersToAverage.length
    );
  }
}

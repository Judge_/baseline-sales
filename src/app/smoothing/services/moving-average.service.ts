import { Injectable } from '@angular/core';
import { AlgorithmType, SmoothingOptions } from '../models';
import { SmoothingAlgorithm } from '../models/smoothing-algorithm.interface';
import { SmoothingFunctionHelpersService } from './smoothing-function-helpers.service';

@Injectable({
  providedIn: 'root',
})
export class MovingAverageService implements SmoothingAlgorithm {
  public algorithmType = AlgorithmType.MovingAverage;
  public displayName = 'Moving Average';
  public usesSmoothingFactor = false;

  public apply(salesData: number[], options: SmoothingOptions): number[] {
    return salesData.map((sales, index) => {
      // Add one to avoid fencepost errors
      const periodIndex = index + 1;

      // We want the smaller of the period and index so we don't throw out the first few values
      const numberOfPeriodsToCheck = Math.min(periodIndex, options.periods);

      const periodsToCheck = salesData.slice(
        periodIndex - numberOfPeriodsToCheck,
        periodIndex
      );

      console.log(
        `Index ${index} (${sales}) has ${numberOfPeriodsToCheck} periods to check: ${periodsToCheck.toString()}`
      );

      return Math.max(
        this.smoothingFunctionHelpersService.getAverage(periodsToCheck),
        0
      );
    });
  }

  constructor(
    private readonly smoothingFunctionHelpersService: SmoothingFunctionHelpersService
  ) {}
}

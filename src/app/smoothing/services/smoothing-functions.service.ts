import { Injectable } from '@angular/core';
import { ParsedSalesInput } from 'src/app/shared/models';
import { AlgorithmType, SmoothingOptions } from '../models';
import { SmoothingAlgorithm } from '../models/smoothing-algorithm.interface';
import { MovingAverageService } from './moving-average.service';
import { SingleExponentialService } from './single-exponential.service';

@Injectable({
  providedIn: 'root',
})
export class SmoothingFunctionsService {
  private smoothingAlgorithms: SmoothingAlgorithm[] = [];

  constructor(
    private readonly movingAverageService: MovingAverageService,
    private readonly singleExponentialService: SingleExponentialService
  ) {
    this.smoothingAlgorithms.push(
      movingAverageService,
      singleExponentialService
    );
  }

  public getSmoothingAlgorithms(): SmoothingAlgorithm[] {
    return this.smoothingAlgorithms;
  }

  public processAndSmoothSales(
    csvData: ParsedSalesInput[],
    algorithmType: AlgorithmType,
    options: SmoothingOptions
  ): number[] {
    const salesValues: number[] = csvData.map((salesInput) => salesInput.Sales);
    const smoothedValues: number[] = [];

    const smoothingAlgorithm = this.smoothingAlgorithms.find(
      (algorithm) => algorithm.algorithmType === algorithmType
    );

    if (!smoothingAlgorithm) {
      throw new Error(`Smoothing algorithm ${algorithmType} not found`);
    }

    smoothedValues.push(...smoothingAlgorithm.apply(salesValues, options));

    return smoothedValues;
  }
}

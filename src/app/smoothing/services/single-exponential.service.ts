import { Injectable } from '@angular/core';
import { AlgorithmType, SmoothingOptions } from '../models';
import { SmoothingAlgorithm } from '../models/smoothing-algorithm.interface';
import { SmoothingFunctionHelpersService } from './smoothing-function-helpers.service';

@Injectable({
  providedIn: 'root',
})
export class SingleExponentialService implements SmoothingAlgorithm {
  public algorithmType = AlgorithmType.SingleExponential;
  public displayName = 'Single Exponential';
  public usesSmoothingFactor = true;

  public apply(salesData: number[], options: SmoothingOptions): number[] {
    const smoothingFactor = options.smoothingFactor ?? 0;
    const smoothedValues: number[] = [];

    salesData.forEach((sales, index) => {
      // We treat the first input differently because it doesn't have a previous value
      if (index === 0) {
        // We want the smaller of the period and index just incase it's a really small dataset
        const numberOfPeriodsToCheck = Math.min(
          salesData.length,
          options.periods
        );

        const periodsToCheck = salesData.slice(0, numberOfPeriodsToCheck);

        smoothedValues.push(
          Math.max(
            this.smoothingFunctionHelpersService.getAverage(periodsToCheck),
            0
          )
        );
      } else {
        smoothedValues.push(
          Math.max(
            sales * smoothingFactor +
              (1 - smoothingFactor) * smoothedValues[index - 1],
            0
          )
        );
      }
    });

    return smoothedValues;
  }

  constructor(
    private readonly smoothingFunctionHelpersService: SmoothingFunctionHelpersService
  ) {}
}

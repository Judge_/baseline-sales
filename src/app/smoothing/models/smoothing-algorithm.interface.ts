import { AlgorithmType } from './algorithm-type.enum';
import { SmoothingOptions } from './smoothing-options.interface';

export interface SmoothingAlgorithm {
  algorithmType: AlgorithmType;
  displayName: string;
  usesSmoothingFactor: boolean;
  apply(salesData: number[], options: SmoothingOptions): number[];
}

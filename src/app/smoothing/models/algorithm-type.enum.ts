export enum AlgorithmType {
  MovingAverage = 'moving-average',
  SingleExponential = 'single-exponential',
}

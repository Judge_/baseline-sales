import { Injectable } from '@angular/core';
import * as saveAs from 'file-saver';
import { BaselineOutput } from 'src/app/smoothing/models';
import { ParsedSalesInput } from '../models';

@Injectable({
  providedIn: 'root',
})
export class ExportCsvService {
  public exportBaselineData(
    parsedValues: ParsedSalesInput[],
    baselineValues: number[],
    originalFilePath: string | null
  ): void {
    const filenameRegex = /(\w+)(?:.csv)/g;
    const parsedBaselineValues: BaselineOutput[] = parsedValues.map(
      (salesData, index) => {
        return {
          Month: salesData.Month,
          Baseline: baselineValues[index].toFixed(2),
        };
      }
    );

    console.log('Baseline Object: ', parsedBaselineValues);

    const header = Object.keys(parsedBaselineValues[0]);
    const csv = parsedBaselineValues.map((row) =>
      header
        .map((fieldName) => row[fieldName as keyof BaselineOutput])
        .join(',')
    );
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    let newFilename = 'baseline.csv';

    if (originalFilePath) {
      const regexResult = filenameRegex.exec(originalFilePath);

      if (!regexResult) {
        throw new Error('Regex Failed');
      }

      const originalFilename = regexResult[1];
      newFilename = `${originalFilename}-baseline.csv`;
    }

    const blob = new Blob([csvArray], { type: 'text/csv' });
    saveAs(blob, newFilename);
  }
}

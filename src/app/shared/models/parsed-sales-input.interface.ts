export interface ParsedSalesInput {
  Month: string;
  Sales: number;
}
